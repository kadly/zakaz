class User < ActiveRecord::Base
  has_many :rides, dependent: :destroy
	before_save { self.phone = phone }
	before_create :create_remember_token
	validates :name, presence: true, length: { maximum: 50 }
	validates :lastname, presence: true, length: { maximum: 50 }
	VALID_PHONE_REGEX = /\A[\d|\+|\(]+[\)|\d|\s|-]*[\d]\z/
	validates :phone, presence: true, format: { with: VALID_PHONE_REGEX }, uniqueness: true
	validates :settlement, presence: true
	has_secure_password
	validates :password, length: { minimum: 6 }

  def User.new_remember_token
    SecureRandom.urlsafe_base64
  end

  def User.encrypt(token)
    Digest::SHA1.hexdigest(token.to_s)
  end

  private

    def create_remember_token
      self.remember_token = User.encrypt(User.new_remember_token)
    end
    
end
