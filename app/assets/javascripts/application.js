// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require turbolinks
//= require_tree .

//= require bootstrap-datepicker

$(document).ready(function () {
      $('#calendar').datepicker({
    language: "ru",
    todayHighlight: true,
    endDate: "+1m",
    startDate: "+1d" 
    });
});

$(document).ready(function () {
    $('#where_city').click(function () {
        $('#div2').hide('slow');
        $('#div1').show('slow');
    });
    $('#where_village').click(function () {
        $('#div1').hide('slow');
        $('#div2').show('slow');
    });
});

$(document).ready(function() {
	$('#where_city').hover(function() {
		$('#div1').toggleClass('highlighted');
	});
	$('#where_village').hover(function() {
		$('#div2').toggleClass('highlighted');
	});
});


//$(document).ready(function() {
//	$('#row tr').click(function() {
//		var href=$(this).find("a").attr("href");
//		if(href) {
//			window.location=href;
//		}
//	});
//});