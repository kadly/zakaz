class CreateRecords < ActiveRecord::Migration
  def change
    create_table :records do |t|
      t.datetime :travel_time
      t.integer :number_of_seats
      t.string :settlement
      t.string :destination

      t.timestamps
    end
  end
end
